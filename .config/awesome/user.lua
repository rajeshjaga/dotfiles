local M={}

M.terminal="LIBGL_ALWAYS_SOFTWARE=1 alacritty"
M.browser="firefox"
M.editor="nvim"
M.menurun="dmenu_run -fn 'FiraCode-14' -nb '#1e1e2e' -nf '#cdd6f4' -sf '#1e1e2e' -sb '#89decb' -l 5 -i -p 'Yellow? > ' "

return M
